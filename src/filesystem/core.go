package filesystem

import (
	"io/ioutil"
	"strings"
)

func removeNewLineChar(str string) string {
	return strings.Replace(str, "\n", "", -1)
}

func GetSource(filePath string) (string, error) {
	stream, err := ioutil.ReadFile(filePath)
	if err != nil {
		return "", err
	}
	return removeNewLineChar(string(stream)), nil
}
