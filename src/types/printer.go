package types

import (
	"fmt"
	"strings"
)

func ListToString(lst []MalType, pr bool,
	start string, end string, join string) string {
	str_list := make([]string, 0, len(lst))
	for _, e := range lst {
		str_list = append(str_list, ExpToString(e, pr))
	}
	return start + strings.Join(str_list, join) + end
}

func ExpToString(obj MalType, print_readably bool) string {
	switch tobj := obj.(type) {
	case List:
		return ListToString(tobj.Val, print_readably, "(", ")", " ")
	case Vector:
		return ListToString(tobj.Val, print_readably, "[", "]", " ")
	case HashMap:
		str_list := make([]string, 0, len(tobj.Val)*2)
		for k, v := range tobj.Val {
			str_list = append(str_list, ExpToString(k, print_readably))
			str_list = append(str_list, ExpToString(v, print_readably))
		}
		return "{" + strings.Join(str_list, " ") + "}"
	case string:
		if strings.HasPrefix(tobj, "\u029e") {
			return ":" + tobj[2:len(tobj)]
		} else if print_readably {
			return `"` + strings.Replace(
				strings.Replace(
					strings.Replace(tobj, `\`, `\\`, -1),
					`"`, `\"`, -1),
				"\n", `\n`, -1) + `"`
		} else {
			return tobj
		}
	case Symbol:
		return tobj.Val
	case nil:
		return "nil"
	case MalFunc:
		return "(fn* " +
			ExpToString(tobj.Params, true) + " " +
			ExpToString(tobj.Exp, true) + ")"
	case func([]MalType) (MalType, error):
		return fmt.Sprintf("<function %v>", obj)
	case *Atom:
		return "(atom " +
			ExpToString(tobj.Val, true) + ")"
	default:
		return fmt.Sprintf("%v", obj)
	}
}
