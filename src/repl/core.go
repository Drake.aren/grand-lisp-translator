package repl

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

import (
	"lexer"
	. "types"
)

func readInput(str string) (MalType, error) {
	return lexer.Tokenize(str)
}

func eval(ast MalType, env string) (MalType, error) {
	fmt.Printf("%#v", ast)
	return ast, nil
}

func show(exp MalType) (string, error) {
	return ExpToString(exp, true), nil
}

func repl(str string) (MalType, error) {
	var exp MalType
	var result string
	var err error
	if exp, err = readInput(str); err != nil {
		return nil, err
	}
	if exp, err = eval(exp, ""); err != nil {
		return nil, err
	}
	if result, err = show(exp); err != nil {
		return nil, err
	}
	return result, nil
}

func Run() {
	reader := bufio.NewReader(os.Stdin)
	for {
		fmt.Print("user>")
		text, err := reader.ReadString('\n')
		if err != nil {
			return
		}
		text = strings.TrimRight(text, "\n")

		var out MalType
		if out, err = repl(text); err != nil {
			if err.Error() == "<empty line>" {
				continue
			}
			fmt.Printf("Error: %v\n", err)
			continue
		}
		fmt.Printf("%v\n", out)
	}
}
