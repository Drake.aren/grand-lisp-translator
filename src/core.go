package main

import (
	"os"
)

import (
	"repl"
	"translate"
)

func main() {
	switch os.Args[1] {
	case "repl":
		repl.Run()
	case "translate":
		translate.Run(os.Args[2])
	}
}
