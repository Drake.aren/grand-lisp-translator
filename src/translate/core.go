package translate

import (
	"fmt"
)

import (
	"filesystem"
)

func Run(filePath string) {
	source, err := filesystem.GetSource(filePath)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s", source)
}
