export GOPATH := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

all:
	go build -o lisp ./src
